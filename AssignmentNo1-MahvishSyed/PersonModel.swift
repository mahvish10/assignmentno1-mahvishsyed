//
//  PersonModel.swift
//  AssignmentNo1-MahvishSyed
//
//  Created by Mahvish Syed on 8/10/21.
//

import Foundation

/// Hold information about person : name, age and gender
struct Person{
    var name : String
    var age : Int?
    var gender : Gender
    
    func description() -> String{
        return "Name : \(self.name), Age : \(String(describing: self.age ?? nil)), Gender : \(gender.description())"
    }
}


/// Information about gender : male or female
enum Gender : String{
    case Male = "male"
    case Female = "female"
    
    func description() -> String{
        return rawValue
    }
}

/// Error messages
enum ErrorMessage : String {
    case NoInformation = "No information available for Preview"
    case InvalidIndex = "Invalid index or Person information does not exsist for given index"
    case RequiredFields = "Required Fields -- Please Enter name and Gender as: Male or Female"
}
