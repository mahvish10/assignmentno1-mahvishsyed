//
//  ViewController.swift
//  AssignmentNo1-MahvishSyed
//
//  Created by Mahvish Syed on 8/10/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var ageTextField: UITextField!
    
    @IBOutlet weak var genderTextField: UITextField!
    
    @IBOutlet weak var indexTextField: UITextField!
    
    var personArray : [Person]?
    var alphabeticallySortedPerson : [Person]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    
    @IBAction func addNewPerson(_ sender: UIButton) {
        checkandAddValidInputs()
    }
    
    
    @IBAction func showOutputOnConsole(_ sender: UIButton) {
        if personArray == nil || personArray?.count == 0 {
            print(ErrorMessage.NoInformation.rawValue)
            return;
        }
        
        print("-----------------------------------------------------------------------------------")
        
        sortPersonByName()
        printInformation(inputArray: alphabeticallySortedPerson ?? nil,
                         message: "\n\n--------------Persons Information Sorted By Name------------")
        
        
        printInformation(inputArray: getSortedArrayByAge() ?? nil,
                         message: "\n\n--------------Persons Information Sorted By Age------------")
        
        print("\n\n-------------Person information sorted by category-------------")
        if let personDict = getDictionaryByAge(){
            for (key, value) in personDict{
                printInformation(inputArray: value, message: key)
            }
        }else{
            print(ErrorMessage.NoInformation.rawValue)
        }
        
       
        print("\n\n------Persons information for given index-------")
        if let indexString = indexTextField.text,
           let validIndex = Int(indexString),
           let person = getPerson(forIndex: validIndex){
                print("-----index : \(indexString)-------")
                print(person.description())
        }else{
            print(ErrorMessage.InvalidIndex.rawValue)
        }
        
    }
}

// MARK : initial setup and input handling
extension ViewController {
    
    /// This method initialize person array
    /// initialize alphabetically sorted person array
    /// Setup delegates for textfields : name, age, gender and index
    private func initialSetup(){
        personArray = [Person]()
        alphabeticallySortedPerson = [Person]()
       
        ageTextField.delegate = self
        nameTextField.delegate = self
        genderTextField.delegate = self
        indexTextField.delegate = self
        
    }
    
    /// This method checks if required field : name and gender are provided
    /// if not print error message and returns
    /// Else add new person information into person array.
    private func checkandAddValidInputs(){
        guard let name = nameTextField.text,
              let gender = genderTextField.text,
              (gender.compare("male", options: .caseInsensitive) == .orderedSame ||
               gender.compare("female", options: .caseInsensitive) == .orderedSame)
        else {
            print(ErrorMessage.RequiredFields.rawValue)
            return
        }
        
        var personAge : Int?
        if let ageString = ageTextField.text,
           let age = Int(ageString){
            personAge = age
        }
        
        var personGender = Gender.Female
        if gender.compare("male", options: .caseInsensitive) == .orderedSame{
            personGender = Gender.Male
        }
       
    
        personArray?.append(Person(name: name, age: personAge ?? nil, gender: personGender))
        
        clearTextFields()
    }
    
    
    /// This method clears all textfield after successfully added person information into person array.
    private func clearTextFields(){
        ageTextField.text?.removeAll()
        nameTextField.text?.removeAll()
        genderTextField.text?.removeAll()
        indexTextField.text?.removeAll()
    }

}

// MARK : Keyboard handling
extension ViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
}

extension ViewController {
    
    /// This method sort person array by person's name in alphabetically order.
    private func sortPersonByName() {
        alphabeticallySortedPerson = personArray?.sorted(by: { $0.name < $1.name })
    }
    
    
    /// This method sort person array by person's age(lowest -> higehst)
    /// If age is nil, it is shown on top
    /// - Returns: sorted person array by age
    private func getSortedArrayByAge() -> [Person]?{
        var sortedByAgePersonArray = personArray?.filter { $0.age == nil}
        
        let otherSortedByAge = personArray?
                        .filter { $0.age != nil}
                        .sorted(by: { $0.age! < $1.age! })
        
        sortedByAgePersonArray?.append(contentsOf: otherSortedByAge ?? [])
        
        return sortedByAgePersonArray
    }
    
    
    /// Categorize person by age into kids, youngadult, adult, seniors
    /// - Returns: dictionary with key as category and value as list of persons under that category
    private func getDictionaryByAge() -> [String : [Person]]?{
        var kids = [Person]()
        var youndAdult = [Person]()
        var adult = [Person]()
        var senior = [Person]()
        var others = [Person]()
        
        for person in personArray!{
            if let age = person.age{
                switch age {
                case 1...17:
                    kids.append(person)
                case 18...35:
                    youndAdult.append(person)
                case 36...55:
                    adult.append(person)
                default:
                    senior.append(person)
                }
            }else{
                others.append(person)
            }
           
        }
        
        var categoryDict = [String : [Person]]()
        categoryDict["others"] = others
        categoryDict["kids"] = kids
        categoryDict["youndAdult"] = youndAdult
        categoryDict["adult"] = adult
        categoryDict["senior"] = senior
        
        return categoryDict
    }
    
    /// Checks if provided index is valid or not
    /// if true, return person's information at that index from alphabetically sorted array
    /// Else print error message
    /// - Parameter index: of which person information needs to be display
    /// - Returns: Person's information at given index
    private func getPerson(forIndex index : Int) -> Person?{
        guard let validArray = alphabeticallySortedPerson,
              let validIndex = index as? Int,
              (validIndex >= 0 && validIndex < validArray.count)
        else{
                print("Index out of range")
                return nil
        }
        
        return validArray[validIndex]
    }
    
    /// It prints person's information of given person array
    /// - Parameters:
    ///   - inputArray: Array of person to print on console
    ///   - message: Descriptive message about what information are displayed.
    private func printInformation(inputArray : [Person]?, message : String){
        
        guard let array = inputArray
        else{
            print(ErrorMessage.NoInformation.rawValue)
            return
        }
        
        print(message)
        
        for person in array {
            print(person.description())
        }
    }

}

